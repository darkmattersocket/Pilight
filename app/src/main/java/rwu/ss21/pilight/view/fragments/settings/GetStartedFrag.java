package rwu.ss21.pilight.view.fragments.settings;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.card.MaterialCardView;

import rwu.ss21.pilight.R;

public class GetStartedFrag extends Fragment implements View.OnClickListener{

    private TextView raspilink, reicheltlink, netzteillink, backendlink, adafruitlink, tutoriallink, ytlink;
    private ConstraintLayout hardware_hidden, software_hidden, hardware, software;;
    private boolean hardware_focused=false, software_focused=false;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.get_started, container, false);

        hardware = root.findViewById(R.id.hardware_fixed);
        hardware_hidden = root.findViewById(R.id.hardware_hidden);
        software = root.findViewById(R.id.software_fixed);
        software_hidden = root.findViewById(R.id.software_hidden);

        raspilink = root.findViewById(R.id.raspilink);
        raspilink.setMovementMethod(LinkMovementMethod.getInstance());

        reicheltlink = root.findViewById(R.id.reicheltlink);
        reicheltlink.setMovementMethod(LinkMovementMethod.getInstance());

        netzteillink = root.findViewById(R.id.netzteillink);
        netzteillink.setMovementMethod(LinkMovementMethod.getInstance());

        backendlink = root.findViewById(R.id.backendlink);
        backendlink.setMovementMethod(LinkMovementMethod.getInstance());

        adafruitlink = root.findViewById(R.id.adafruitlink);
        adafruitlink.setMovementMethod(LinkMovementMethod.getInstance());

        tutoriallink = root.findViewById(R.id.tutoriallink);
        tutoriallink.setMovementMethod(LinkMovementMethod.getInstance());

        ytlink = root.findViewById(R.id.ytlink);
        ytlink.setMovementMethod(LinkMovementMethod.getInstance());

        hardware.setOnClickListener(this);
        software.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.hardware_fixed:
                if(hardware_focused)
                    hardware_hidden.setVisibility(View.GONE );
                else
                    hardware_hidden.setVisibility(View.VISIBLE);
                hardware_focused =! hardware_focused;
                break;
            case R.id.software_fixed:
                if(software_focused)
                    software_hidden.setVisibility(View.GONE );
                else
                    software_hidden.setVisibility(View.VISIBLE);
                software_focused =! software_focused;
                break;
            default: break;
        }
    }
}
