package rwu.ss21.pilight.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.service.repository.LEDRepository;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.viewmodel.ServerSettingVM;

import com.google.android.material.slider.Slider;

public class LightEffectFrag extends Fragment implements View.OnClickListener{

    private View root;
    private static MenuItem active_server;
    private ServerSettingVM serverSettingVM;
    private ConstraintLayout rainlayout,bouncelayout;
    private ImageView rainimg, bounceimg;

    private int width, height;

    private boolean focused = false;

    private Slider effectslider;
    private TextView effectslider_tv, effectslider_tv_param;

    private  String effect;

    private LEDRepository ledRepository;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout._alighteffects, container, false);
        setHasOptionsMenu(true);
        ledRepository = LEDRepository.getInstance();


        rainlayout = root.findViewById(R.id.rainlayout);
        rainimg = root.findViewById(R.id.rainimg);

        width = rainimg.getLayoutParams().width;
        height = rainimg.getLayoutParams().height;

        bouncelayout = root.findViewById(R.id.bouncelayout);
        bounceimg = root.findViewById(R.id.bounceimg);

        rainimg.setOnClickListener(this);
        bounceimg.setOnClickListener(this);
        rainlayout.setOnClickListener(this);
        bouncelayout.setOnClickListener(this);

        effectslider = root.findViewById(R.id.effectslider);
        effectslider_tv = root.findViewById(R.id.effectslider_tv);
        effectslider_tv_param = root.findViewById(R.id.effectslider_tv_param);

        effectslider.setLabelFormatter(speed -> {
            String val = String.valueOf((int)speed);
            effectslider_tv_param.setText(val);
            ledRepository.requestEffect(effect, val);
            return val;
        });


        return root;
    }

    @Override
    public void onClick(View v) {
        boolean bingo = false;
        ConstraintLayout hiddenView = null;


        switch(v.getId())
        {
            case R.id.rainimg: case R.id.rainlayout: case R.id.raincard:
                if(focused) {
                    bouncelayout.setVisibility(View.VISIBLE);
                    setImgSize(rainimg, width, height);
                }
                else{
                    bouncelayout.setVisibility(View.GONE);
                    effectslider.setValueFrom(1);
                    effectslider.setValueTo(100);
                    effectslider.setValue(50);
                    effectslider_tv.setText("Speed: ");
                    effect = "Rainbow";
                    setImgSize(rainimg, width*2, height*2);

                }
            rainlayout.setVisibility(View.VISIBLE);
            focused = !focused;
                break;
            case R.id.bounceimg: case R.id.bouncelayout: case R.id.bouncecard:
                if(focused){
                    rainlayout.setVisibility(View.VISIBLE);
                    setImgSize(bounceimg, width, height);
                }
                else{
                    rainlayout.setVisibility(View.GONE);
                    effectslider.setValueFrom(1);
                    effectslider.setValueTo(10);
                    effectslider.setValue(5);
                    effectslider_tv.setText("Balls: ");
                    effect = "Bouncing Balls";
                    setImgSize(bounceimg, width*2, height*2);
                }
                bouncelayout.setVisibility(View.VISIBLE);
                focused = !focused;
                break;
            default: break;
        }
        hiddenView = root.findViewById(R.id.hidden_layout);
        bingo = true;

        if (bingo && hiddenView!=null){
            //TransitionManager.beginDelayedTransition(card_base, new AutoTransition());
            if (hiddenView.getVisibility() == View.VISIBLE )
            {
                hiddenView.setVisibility(View.GONE);
            }
            else{
                hiddenView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setImgSize(ImageView img, int w, int h){
        img.getLayoutParams().width = w;
        img.getLayoutParams().height = h;
        img.requestLayout();
    }
    @Override // SETTINGS Icon oben rechts wurde geklickt
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return Validator.Clicker(item, getActivity().getSupportFragmentManager());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.select_server_menu, menu);
        active_server = menu.findItem(R.id.active_server);
        serverSettingVM = new ViewModelProvider(this).get(ServerSettingVM.class);

        // zeigt den aktuell ausgewählten server an
        serverSettingVM.getAllServers().observe(getViewLifecycleOwner(), servers -> {
            active_server.setTitle(serverSettingVM.get_active_server_name());
            serverSettingVM.set_active_BASEURL();
        });
    }

}