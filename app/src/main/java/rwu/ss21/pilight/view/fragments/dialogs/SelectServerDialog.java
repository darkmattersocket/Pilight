package rwu.ss21.pilight.view.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.adapter.ServerAdapter;
import rwu.ss21.pilight.models.Server;
import rwu.ss21.pilight.service.repository.AlarmRepository;
import rwu.ss21.pilight.service.repository.LEDRepository;
import rwu.ss21.pilight.service.rest.RetrofitManager;
import rwu.ss21.pilight.viewmodel.ServerSettingVM;
import com.google.android.material.button.MaterialButton;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class SelectServerDialog extends DialogFragment implements LEDRepository.OnLED_ErrorListener, LEDRepository.OnLED_ResponseListener{

    private AlertDialog dialog;
    private ServerAdapter adapter;
    private Server selected_server;
    private static SelectServerDialog instance;
    private final LEDRepository ledRepository;
    private final RetrofitManager retrofitmanager;
    private static Context context =null;
    private static FragmentActivity activity =null;
    private FragmentManager fragmentManager;
    private ServerSettingVM serverSettingVM;

    private SelectServerDialog(){
        retrofitmanager = RetrofitManager.getInstance();
        ledRepository = LEDRepository.getInstance();
    }

    public static SelectServerDialog getInstance(){
        if(instance==null)
            instance = new SelectServerDialog();
        return instance;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        context = getActivity();
        activity = getActivity();
        fragmentManager = activity.getSupportFragmentManager();
        LayoutInflater inflater = LayoutInflater.from(context);
        View selectserverdialogview = inflater.inflate(R.layout.dialog_select_server, null);

        // Set title
        TextView title = selectserverdialogview.findViewById(R.id.topbar_title);
        title.setText("Select your server");

        // RECYCLER VIEW
        RecyclerView recyclerView = selectserverdialogview.findViewById(R.id.server_rec_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        // Set the Server-ADAPTER
        adapter = new ServerAdapter(R.layout.item_server_radio);
        recyclerView.setAdapter(adapter);

        // ViewModel
        serverSettingVM = new ViewModelProvider(this).get(ServerSettingVM.class);
        serverSettingVM.getAllServers().observe(this, servers -> {
            adapter.submitList(servers);
        }  );

        selected_server = serverSettingVM.get_active_server();
        if(selected_server != null) {
            adapter.itemCheckChanged(selected_server.getId());
            retrofitmanager.setBASE_URL(selected_server.getIp(), String.valueOf(selected_server.getPort()));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(selectserverdialogview);
        MaterialButton add_btn = selectserverdialogview.findViewById(R.id.add_item_btn);
        MaterialButton cancel_btn = selectserverdialogview.findViewById(R.id.close_btn);
        dialog = builder.create();

        // Um die ecken abzurunden bzw unsichtbar zu machen, damit man die RUndungen sieht
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        add_btn.setOnClickListener(v -> {
            AddServerDialog addDialog = new AddServerDialog();
            addDialog.show(activity.getSupportFragmentManager(), "Edit Server TAG");
        });
        cancel_btn.setOnClickListener(v -> {
            dialog.dismiss();
        });

        // zum swipen/löschen der server items
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback
                (0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Server swiped_server = adapter.getServerAt(viewHolder.getAdapterPosition());
                Toast.makeText(activity, "Server " +swiped_server.getName() +" deleted", Toast.LENGTH_SHORT).show();
                serverSettingVM.delete(adapter.getServerAt(viewHolder.getAdapterPosition()));
                if(selected_server == swiped_server) {
                    reset_to_nothing_selected();
                }
            }
        }).attachToRecyclerView(recyclerView);

        // ein server wurde ausgewählt
        adapter.setOnServerClickListener((server) -> {
            if(serverSettingVM.get_active_server() == server)
                return;
            selected_server = server;
            ledRepository.setOnLED_ErrorListener(this);
            ledRepository.setOnLED_ResponseListener(this);
            retrofitmanager.setBASE_URL(server.getIp(), String.valueOf(server.getPort()));
            AlarmRepository.getInstance().GETAlarms();
        });

        return dialog;
    }

    @Override
    public void onLED_Response(Response<ResponseBody> ledResponse) {
        ledRepository.removeOnLED_ResponseListener(this);
        ledRepository.removeOnLED_ErrorListener(this);
        if(ledResponse.code() < 300)
        {
            //Log.e("SERVER EXISTS", String.valueOf(ledResponse.code()));
            serverSettingVM.setActiveServer(selected_server);
            adapter.itemCheckChanged(selected_server.getId());
            dialog.dismiss();
        }
    }
    private void reset_to_nothing_selected(){
        serverSettingVM.clear_active_server();
        adapter.itemCheckChanged(-1);
        retrofitmanager.setBASE_URL("0.0.0.0", "1234");
        selected_server = null;
    }
    @Override
    public void onLED_Error(Throwable errorMsg) {
        // deaktiviere alle server
        ledRepository.removeOnLED_ErrorListener(this);
        ledRepository.removeOnLED_ResponseListener(this);
        reset_to_nothing_selected();
        ErrorInfoDialog errorDialog = new ErrorInfoDialog(errorMsg.getMessage(), context);
        errorDialog.show(fragmentManager, "Error Dialog TAG");
    }
}
