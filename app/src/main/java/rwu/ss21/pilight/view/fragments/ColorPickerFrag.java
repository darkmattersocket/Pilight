package rwu.ss21.pilight.view.fragments;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import rwu.ss21.pilight.MainActivity;
import rwu.ss21.pilight.R;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.viewmodel.ColorPickerVM;
import rwu.ss21.pilight.viewmodel.ServerSettingVM;

import top.defaults.colorpicker.ColorPickerView;

public class ColorPickerFrag extends Fragment  {

    private ColorPickerVM colorPickerVM;
    private ColorPickerView color_wheel;
    private TextView response_msg, response_code, red, green, blue, hex;
    private static MenuItem active_server;
    private ServerSettingVM serverSettingVM;
    private static OnColorChangedListener2 onColorChangedListener;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout._colorpicker, container, false);
        setHasOptionsMenu(true);
        // View Elemente
        colorPickerVM = new ViewModelProvider(this).get(ColorPickerVM.class);

        response_msg = root.findViewById(R.id.response_msg);
        response_code = root.findViewById(R.id.response_code);
        red = root.findViewById(R.id.red);
        green = root.findViewById(R.id.green);
        blue = root.findViewById(R.id.blue);
        hex = root.findViewById(R.id.hex);

        color_wheel = root.findViewById(R.id.colorwheel);

        color_wheel.setInitialColor(-1);
        color_wheel.subscribe( // ausgewählte Farbe wird direkt zum Viewmodel geschickt
                (color, fromUser, shouldPropagate) -> colorPickerVM.process_and_request_SelectedColor(color)
        );


        // sobald der Pi antwortet, geht die Antwort vom repository zum viewmodel,
        // und hier wird die Antwort observiert und direkt angezeigt
        colorPickerVM.get_LED_LIVE_DATA().observe(getViewLifecycleOwner(),
                _LED_LIVE_DATA_ -> {
                    int color = _LED_LIVE_DATA_.getCurrentColor();

                    response_msg.setText(_LED_LIVE_DATA_.getResponse_msg());
                    response_code.setText(String.valueOf(_LED_LIVE_DATA_.getResponse_code()));

                    red.setText(_LED_LIVE_DATA_.getRed());
                    green.setText(_LED_LIVE_DATA_.getGreen());
                    blue.setText(_LED_LIVE_DATA_.getBlue());
                    hex.setText(_LED_LIVE_DATA_.getHex() + " ("+color+")");
                    hex.setTextColor(color);

                    if(this.onColorChangedListener!=null)
                        this.onColorChangedListener.onColorChanged2(color);
                }
         );

        return root;
    }

    public interface OnColorChangedListener2 {
        void onColorChanged2(int color);
    }
    public static void setOnColorChangedListener2(OnColorChangedListener2 listener){
        onColorChangedListener = listener;
    }
    public static void removeOnColorChangedListener2(OnColorChangedListener2 listener){
        onColorChangedListener = null;
    }

    @Override // SETTINGS Icon oben rechts wurde geklickt
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return Validator.Clicker(item, getActivity().getSupportFragmentManager());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.select_server_menu, menu);
        active_server = menu.findItem(R.id.active_server);
        serverSettingVM = new ViewModelProvider(this).get(ServerSettingVM.class);

        // zeigt den aktuell ausgewählten server an
        serverSettingVM.getAllServers().observe(getViewLifecycleOwner(), servers -> {
            active_server.setTitle(serverSettingVM.get_active_server_name());
            serverSettingVM.set_active_BASEURL();
        });
    }

}