package rwu.ss21.pilight.view.fragments.schedules;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.adapter.AlarmAdapter;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.viewmodel.ServerSettingVM;
import rwu.ss21.pilight.viewmodel.schedules.AddAlarmVM;
import rwu.ss21.pilight.viewmodel.schedules.DetailsVM;

public class DetailsFrag extends Fragment implements View.OnClickListener{

    private static final String TAG = "DetailsFrag";
    private DetailsVM detailsVM;
    private AddAlarmVM sharedAlarmVM;
    private View sunriseView;
    private AlarmAdapter adapter;

    private static MenuItem active_server;
    private ServerSettingVM serverSettingVM;
    private Button edit_btn, add_btn;
    private Bundle packet;
    private static int alarmtype;
    private static String title;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        sunriseView = inflater.inflate(R.layout.schedule_details, container, false);

        packet = getArguments();
        if(packet != null){
            //Log.e(TAG, packet.toString());
            alarmtype = packet.getInt("type");
            title = packet.getString("title");
            TextView title_tv = sunriseView.findViewById(R.id.topbar_title);
            title_tv.setText(title);
        }


        // Recyclerview
        RecyclerView recView = sunriseView.findViewById(R.id.schedules_recview);
        recView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Alarm-Adapter
        adapter = new AlarmAdapter(R.layout.item_alarm);
        recView.setAdapter(adapter);

        detailsVM = new ViewModelProvider(this).get(DetailsVM.class);
        sharedAlarmVM = new ViewModelProvider(requireActivity()).get(AddAlarmVM.class);

        add_btn = sunriseView.findViewById(R.id.add_item_btn);
        edit_btn = sunriseView.findViewById(R.id.edit_item_btn);
        edit_btn.setOnClickListener(this);
        add_btn.setOnClickListener(this);
        return sunriseView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        detailsVM.get_ALARM_LIVEDATA().observe(getViewLifecycleOwner(), alarmlist -> {
            if(alarmlist!= null){
                List<Alarm> extracted_alarms = new ArrayList<>();
                for (Alarm alarm : alarmlist){
                    if(alarm.getType() == alarmtype ){
                        extracted_alarms.add(alarm);
                    }
                }
                adapter.submitList(extracted_alarms);
            }
            else
                adapter.submitList(null);
        });
    }

    @Override // SETTINGS Icon oben rechts wurde geklickt
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        Validator.Clicker(item, getActivity().getSupportFragmentManager());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.select_server_menu, menu);
        active_server = menu.findItem(R.id.active_server);
        serverSettingVM = new ViewModelProvider(this).get(ServerSettingVM.class);

        // zeigt den aktuell ausgewählten server an
        serverSettingVM.getAllServers().observe(getViewLifecycleOwner(), servers -> {
            active_server.setTitle(serverSettingVM.get_active_server_name());
            serverSettingVM.set_active_BASEURL();
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.edit_item_btn:
                    if (adapter.isEditing()) {
                        edit_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_done, 0, 0, 0);
                    } else {
                        edit_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_edit, 0, 0, 0);
                    }
                break;

            case R.id.add_item_btn:
                    sharedAlarmVM.setDefault(alarmtype);
                    Navigation.findNavController(sunriseView).navigate(R.id.add_alarm_frag);
                break;
            default: break;
        }
    }
}