package rwu.ss21.pilight.view.fragments.schedules;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import rwu.ss21.pilight.R;

public class SchedulesFrag extends Fragment implements View.OnClickListener {

    private ConstraintLayout home_away, sunrise, sunset, timer, myschedules;
    private View schedules_view;
    private TextView textview;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        schedules_view = inflater.inflate(R.layout._schedule, container, false);

        home_away = schedules_view.findViewById(R.id.home_and_away_layout);
        sunrise = schedules_view.findViewById(R.id.sunrise_layout);
        sunset = schedules_view.findViewById(R.id.sunset_layout);
        timer = schedules_view.findViewById(R.id.timer_layout);
        myschedules = schedules_view.findViewById(R.id.my_schedules_layout);

        return schedules_view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //schedulesVM.getText().observe(getViewLifecycleOwner(), this);

        home_away.setOnClickListener(this);     // TYPE 1
        sunrise.setOnClickListener(this);       // TYPE 2
        sunset.setOnClickListener(this);        // TYPE 3
        timer.setOnClickListener(this);         // TYPE 4
        myschedules.setOnClickListener(this);   // TYPE 5
    }

    @Override
    public void onClick(View v) {
        NavController nav = Navigation.findNavController(schedules_view);
        Bundle b = new Bundle();
        boolean bingo = false;

        switch(v.getId()){

            case R.id.home_and_away_layout:
                b.putString("title", "Home & Away");
                b.putInt("type", 1);
                bingo = true;
                break;
            case R.id.sunrise_layout:
                b.putString("title", "Morningroutine");
                b.putInt("type", 2);
                bingo = true;
                break;
            case R.id.sunset_layout:
                b.putString("title", "Sleeproutine");
                b.putInt("type", 3);
                bingo = true;
                break;
            case R.id.timer_layout:
                b.putString("title", "Timer Routines");
                b.putInt("type", 4);
                bingo = true;
                break;
            case R.id.my_schedules_layout:
                b.putString("title", "Custom Routines");
                b.putInt("type", 5);
                bingo = true;
                break;
            default: break;

        }
        if(bingo)
            nav.navigate(R.id.to_details, b);
    }
}
