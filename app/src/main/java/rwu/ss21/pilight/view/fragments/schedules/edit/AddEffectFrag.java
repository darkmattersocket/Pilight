package rwu.ss21.pilight.view.fragments.schedules.edit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.slider.Slider;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.view.fragments.ColorPickerFrag;
import rwu.ss21.pilight.viewmodel.schedules.AddAlarmVM;
import top.defaults.colorpicker.ColorPickerView;

public class AddEffectFrag extends Fragment implements View.OnClickListener {

    private View effectView;
    private ConstraintLayout color, rainbow, bounce, off, sunrise;
    private int previouscheck =-1;
    private int previousparam = -1;
    private ColorPickerView color_wheel;
    private AddAlarmVM vm;
    private Slider slider;
    private TextView speedsliderTV, speedsliderTVparam;
    private static OnColorChangedListener onColorChangedListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        effectView= inflater.inflate(R.layout.schedule_details_add_effect, container, false);
        vm = new ViewModelProvider(requireActivity()).get(AddAlarmVM.class);

        TextView title = effectView.findViewById(R.id.topbar_title);
        Button edit = effectView.findViewById(R.id.edit_item_btn);
        Button add = effectView.findViewById(R.id.add_item_btn);
        edit.setVisibility(View.GONE);
        add.setVisibility(View.GONE);
        title.setText("Effect");

        color = effectView.findViewById(R.id.color_layout);
        rainbow = effectView.findViewById(R.id.rainbow_layout);
        bounce = effectView.findViewById(R.id.bounce_layout);
        off = effectView.findViewById(R.id.off_layout);
        sunrise = effectView.findViewById(R.id.sunrise_layout);
        color_wheel = effectView.findViewById(R.id.colorwheel);
        speedsliderTV = effectView.findViewById(R.id.speedslider_text);
        speedsliderTVparam = effectView.findViewById(R.id.speedslider_text2);

        slider = effectView.findViewById(R.id.speedslider);

        slider.setLabelFormatter(speed -> {
            String val = String.valueOf((int)speed);
            speedsliderTVparam.setText(val);

            vm.setEffectParam(val);
            return val;
        });
        color.setOnClickListener(this);
        rainbow.setOnClickListener(this);
        bounce.setOnClickListener(this);
        off.setOnClickListener(this);
        sunrise.setOnClickListener(this);
        return effectView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String effect = vm.getEffect().getValue();
        if(effect.equals("Color")) {
            updateUi(R.id.color_check, R.id.color_hidden_layout, effect);

        }
        else if(effect.equals("Rainbow")) {
            updateUi(R.id.rainbow_check, R.id.hidden_speedslider, effect);

        }
        else if(effect.equals("Bouncing Balls")) {
            updateUi(R.id.bounce_check, R.id.hidden_speedslider, effect);
        }
        else if(effect.equals("Off")) {
            updateUi(R.id.off_check, -1, effect);

        }
        else if(effect.equals("Sunrise")){
            updateUi(R.id.sunrise_check, R.id.hidden_speedslider, effect);
        }

        color_wheel.subscribe( (color, fromUser, shouldPropagate) -> {
                    int r = ((color >> 16) & 255);
                    int g = ((color >> 8) & 255);
                    int b = color & 255;
                    String hex_r = Validator.padLeftZeros(Integer.toHexString(r), 2);
                    String hex_g = Validator.padLeftZeros(Integer.toHexString(g), 2);
                    String hex_b = Validator.padLeftZeros(Integer.toHexString(b), 2);
                    String color_in_hexa = hex_r + hex_g + hex_b;
                    vm.setEffectParam(color_in_hexa);

                    if(onColorChangedListener!=null)
                        onColorChangedListener.onColorChanged(color);
                }
        );
    }

    @Override // SETTINGS Icon oben rechts wurde geklickt
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.color_layout:
                updateUi(R.id.color_check, R.id.color_hidden_layout,"Color");
                break;

            case R.id.rainbow_layout:
                updateUi(R.id.rainbow_check, R.id.hidden_speedslider,"Rainbow");
                slider.setValueFrom(1);
                slider.setValueTo(100);
                slider.setValue(50);
                speedsliderTV.setText("Speed: ");
                break;

            case R.id.bounce_layout:
                updateUi(R.id.bounce_check, R.id.hidden_speedslider, "Bouncing Balls");
                slider.setValueFrom(1);
                slider.setValueTo(10);
                slider.setValue(5);
                speedsliderTV.setText("Balls: ");
                break;

            case R.id.off_layout:
                updateUi(R.id.off_check, -1,"Off");
                break;

            case R.id.sunrise_layout:
                updateUi(R.id.sunrise_check, R.id.hidden_speedslider,"Sunrise");
                slider.setValueFrom(0);
                slider.setValueTo(60);
                slider.setValue(30);
                speedsliderTV.setText("Minutes until full brightness: ");
                break;
            default:
                break;
        }
    }
    private void updateUi(int checkId, int paramId, String effectname){
        View item = effectView.findViewById(previouscheck);
        if(item !=null)
            item.setVisibility(View.GONE);
        item = effectView.findViewById(previousparam);
        if(item!=null)
            item.setVisibility(View.GONE);

        item = effectView.findViewById(checkId);
        if (item !=null)
            item.setVisibility(View.VISIBLE);

        item = effectView.findViewById(paramId);
        if(item !=null)
            item.setVisibility(View.VISIBLE);
        previouscheck = checkId;
        previousparam = paramId;
        vm.setEffect(effectname);
    }

    public interface OnColorChangedListener {
        void onColorChanged(int color);
    }
    public static void setOnColorChangedListener(OnColorChangedListener listener){
        onColorChangedListener = listener;
    }
    public static void removeOnColorChangedListener(OnColorChangedListener listener){
        onColorChangedListener = null;
    }
}
