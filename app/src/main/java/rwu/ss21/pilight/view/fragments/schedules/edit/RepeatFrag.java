package rwu.ss21.pilight.view.fragments.schedules.edit;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.viewmodel.schedules.AddAlarmVM;

public class RepeatFrag extends Fragment implements View.OnClickListener {


    private ConstraintLayout mon, tue, wed, thu, fri, sat, sun;
    private View repeatView;
    private int days;
    private TextView daystring;
    private int[] ids;
    private AddAlarmVM vm;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        repeatView= inflater.inflate(R.layout.schedule_details_add_repeat, container, false);
        daystring = repeatView.findViewById(R.id.daystring);

        TextView title = repeatView.findViewById(R.id.topbar_title);
        Button edit = repeatView.findViewById(R.id.edit_item_btn);
        Button add = repeatView.findViewById(R.id.add_item_btn);
        edit.setVisibility(View.GONE);
        add.setVisibility(View.GONE);
        title.setText("Repeat");

        ids = new int[7];
        ids[0] = R.id.monday_check;
        ids[1] = R.id.tuesday_check;
        ids[2] = R.id.wednesday_check;
        ids[3] = R.id.thursday_check;
        ids[4] = R.id.friday_check;
        ids[5] = R.id.saturday_check;
        ids[6] = R.id.sunday_check;

        mon = repeatView.findViewById(R.id.monday_layout);
        tue = repeatView.findViewById(R.id.tuesday_layout);
        wed = repeatView.findViewById(R.id.wednesday_layout);
        thu = repeatView.findViewById(R.id.thursday_layout);
        fri = repeatView.findViewById(R.id.friday_layout);
        sat = repeatView.findViewById(R.id.saturday_layout);
        sun = repeatView.findViewById(R.id.sunday_layout);

        mon.setOnClickListener(this);
        tue.setOnClickListener(this);
        wed.setOnClickListener(this);
        thu.setOnClickListener(this);
        fri.setOnClickListener(this);
        sat.setOnClickListener(this);
        sun.setOnClickListener(this);

        return repeatView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        vm = new ViewModelProvider(requireActivity()).get(AddAlarmVM.class);
        days = vm.getDays().getValue();

        for(int i=0;i<7;i++){
            int comp = (int)Math.pow(2, i);
            if((days & comp) == comp)
                repeatView.findViewById(ids[i]).setVisibility(View.VISIBLE);
        }
        //Log.e("REPEAT", vm.toString());
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.monday_layout:
                checkVisibility( 0);
                break;

            case R.id.tuesday_layout:
                checkVisibility( 1);
                break;

            case R.id.wednesday_layout:
                checkVisibility(2);
                break;

            case R.id.thursday_layout:
                checkVisibility(3 );
                break;

            case R.id.friday_layout:
                checkVisibility(4);
                break;

            case R.id.saturday_layout:
                checkVisibility(5);
                break;

            case R.id.sunday_layout:
                checkVisibility(6);
                break;
            default: break;

        }
    }

    private void checkVisibility(int i){
        int add_remove_amnt  = (int)Math.pow(2, i);

        if(repeatView.findViewById(ids[i]).getVisibility() == View.GONE) {
            days += add_remove_amnt;
            repeatView.findViewById(ids[i]).setVisibility(View.VISIBLE);
        }
        else {
            repeatView.findViewById(ids[i]).setVisibility(View.GONE);
            days -= add_remove_amnt;
        }
        daystring.setText(Validator.get_days_from_code(days));
        vm.setDays(days);
    }
}