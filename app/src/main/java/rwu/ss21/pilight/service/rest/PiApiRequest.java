package rwu.ss21.pilight.service.rest;

import java.util.List;

import okhttp3.Response;
import retrofit2.http.DELETE;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.models.Effect;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PiApiRequest {

    @GET("greeting")
    Call<ResponseBody> GETGreeting();

    @Headers({"Accept: application/json"})
    @POST("effect")
    Call<ResponseBody> POSTeffect(@Body Effect effect);

    @GET("alarm")
    Call<List<Alarm>>  GETAlarms();

    @POST("alarm")
    Call<ResponseBody> CreateAlarm(@Body Alarm alarm);

    @PUT("alarm/{id}")
    Call<ResponseBody> UpdateAlarm(@Path("id") int id, @Body Alarm alarm);

    @DELETE("alarm/{id}")
    Call<ResponseBody>  DELETEAlarm(@Path("id") int id);
}
