package rwu.ss21.pilight.service.sqlite;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rwu.ss21.pilight.models.Alarm;

@Database(entities = {Alarm.class}, version = 1, exportSchema = false)
public abstract class AlarmDatabase extends RoomDatabase {

    public abstract  AlarmDao alarmDao();

    private static volatile AlarmDatabase instance;

    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    // Singleton
    public static synchronized AlarmDatabase getInstance(final Context context){
        if(instance==null)
        {
            synchronized (AlarmDatabase.class) {
                if(instance == null){
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            AlarmDatabase.class, "alarm_database")
                            // wenn das DB-Schema geändert wird, werden alle daten gelöscht, was hier ja nicht schlimm ist
                            .fallbackToDestructiveMigration()
                            // das würde direkt den Alarm "HomeAway" unten in die DB einfügen
                            //.addCallback(roomDatabaseCallback)
                            .build();
                }
            }
        }
        return instance;
    }
    // Ab hier nur beispielhaftes Einfügen in die Server-DB
    private static Callback roomDatabaseCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriteExecutor.execute(() -> {
                AlarmDao dao = instance.alarmDao();
                dao.insert(new Alarm(0, "HomeAway", 1, "09:00", 127, "off", "", false));
            });
        }
    };

}
