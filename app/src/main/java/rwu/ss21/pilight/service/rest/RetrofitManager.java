package rwu.ss21.pilight.service.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rwu.ss21.pilight.models.Alarm;

public class RetrofitManager {
    private Retrofit retrofit;
    private static RetrofitManager manager;
    private OkHttpClient okHttpClient;
    private static String BASE_URL = null;
    private OnURLChangedListener listener;

    // Singleton
    private RetrofitManager(){
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(500, TimeUnit.MILLISECONDS)
                .readTimeout(100, TimeUnit.MILLISECONDS)
                .writeTimeout(100, TimeUnit.MILLISECONDS)
                .build();
    }
    public static RetrofitManager getInstance(){
        if(manager ==null)
            manager = new RetrofitManager();
        return manager;
    }

    public Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            setBASE_URL("0.0.0.0", "8888");
        }
        return retrofit;
    }
    public void setBASE_URL(String _IP, String _PORT) {
        BASE_URL = "http://" + _IP + ":" + _PORT + "/";
        setInstance();
        listener.onURLChanged(retrofit);
    }

    private void setInstance(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit =  new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }
    public interface OnURLChangedListener{
        void onURLChanged(Retrofit retrofit);
    }
    public void setOnURLChangedListener(OnURLChangedListener newlistener){
        listener = newlistener;
    }
}
