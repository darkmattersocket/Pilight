package rwu.ss21.pilight.models;

public class Effect {
    private static Effect instance;
    private String effect, effectparam;

    public static Effect getInstance(String effect, String param){
        if (instance ==null)
        {
            instance = new Effect();
        }
        instance.effect = effect;
        instance.effectparam = param;
        return instance;
    }

    private Effect() {}
}
