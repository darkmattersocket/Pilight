package rwu.ss21.pilight.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

// @Room:  Eine Entity representiert ein Table der SQLITE DB
@Entity(tableName = "server_table")
public class Server {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private String ip;
    private int port;
    boolean isActive;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Server(String name, String ip, int port, boolean isActive) {
        this.name = name;
        this.ip = ip;
        this.port = port;
        this.isActive = isActive;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}