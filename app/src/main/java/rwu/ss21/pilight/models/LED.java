package rwu.ss21.pilight.models;

public class LED {

    private String red, green, blue, hex, response_msg;
    private int current_color, response_code;
    // volatile für atomaren zugriff
    private static volatile LED instance;

    // Singleton
    private LED(){}
    public static synchronized LED getInstance(){
        if(instance==null)
        {
            synchronized (LED.class) {
                if(instance == null){
                    instance = new LED();
                }
            }
        }
        return instance;
    }

    public String getRed() {
        return red;
    }

    public void setRed(String red) {
        this.red = red;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public String getBlue() {
        return blue;
    }

    public void setBlue(String blue) {
        this.blue = blue;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public int getCurrentColor() {
        return current_color;
    }
    public void setCurrentColor(int color) {
        current_color = color;
    }


    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }
    public String getResponse_msg() {
        return response_msg;
    }
}
