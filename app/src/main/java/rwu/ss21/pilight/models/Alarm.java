package rwu.ss21.pilight.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

// @Room:  Eine Entity representiert ein Table der SQLITE DB
@Entity(tableName = "alarm_table")

public class Alarm {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int days;
    private int type;
    private boolean enabled;
    private String name;
    private String time;
    private String effect;
    private String effectparam;

    public Alarm(){}
    public Alarm(int id, String name, int type, String time, int days, String effect, String effectparam, boolean enabled){
        this.id = id;
        this.name = name;
        this.type = type;
        this.time = time;
        this.days = days;
        this.effect = effect;
        this.effectparam = effectparam;
        this.enabled = enabled;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public int getDays() {
        return days;
    }

    public String getName() {
        return name;
    }
    public String getTime(){
        return time;
    }

    public String getEffect() {
        return effect;
    }

    public String getEffectparam() {
        return effectparam;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public void setEffectparam(String effectparam) {
        this.effectparam = effectparam;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
