package rwu.ss21.pilight.viewmodel.schedules;

import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import rwu.ss21.pilight.models.Alarm;

public class AddAlarmVM extends ViewModel {

    private MutableLiveData<Integer> id = new MutableLiveData<>();
    private MutableLiveData<String> name = new MutableLiveData<>();
    private MutableLiveData<Integer> type = new MutableLiveData<>();
    private MutableLiveData<String> time = new MutableLiveData<>();
    private MutableLiveData<Integer> days = new MutableLiveData<>();
    private MutableLiveData<String> effect = new MutableLiveData<>();
    private MutableLiveData<String> effectparam = new MutableLiveData<>();
    private MutableLiveData<Boolean> enabled = new MutableLiveData<>();

    public void setId(Integer input) {
        id.setValue(input);
    }
    public void setName(String input){
        name.setValue(input);
    }
    public void setType(Integer input){
        type.setValue(input);
    }
    public void setTime(String input){
        time.setValue(input);
    }
    public void setDays(Integer input) {
        days.setValue(input);
    }
    public void setEffect(String input) { effect.setValue(input); }
    public void setEffectParam(String input) { effectparam.setValue(input); }
    public void setEnabled(Boolean input){
        enabled.setValue(input);
    }


    public LiveData<Integer> getId() { return id; }
    public LiveData<String> getName() { return name; }
    public LiveData<Integer> getType() { return type; }
    public LiveData<String> getTime() { return time; }
    public LiveData<Integer> getDays() { return days; }
    public LiveData<String> getEffect() {
        return effect;
    }
    public LiveData<String> getEffectParam() { return effectparam; }
    public LiveData<Boolean> getEnabled() { return enabled; }


    public void setDefault(int typed){
        id.setValue(0);
        name.setValue("Alarm");
        type.setValue(typed);
        time.setValue("12:00");
        days.setValue(0);
        effect.setValue("Rainbow");
        effectparam.setValue("");
        enabled.setValue(true);
    }

}